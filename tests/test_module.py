#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: TODO
   :platform: Unix
   :synopsis: TODO.

.. moduleauthor:: Aljosha Friemann aljosha.friemann@gmail.com

"""

import unittest, requests
from unittest import mock

from simple_rest import Endpoint, API, Client

example_api = {
                  'scheduler': {
                      'endpoints': {
                          'jobs': {
                              'methods': {
                                  'get': [200]
                              }
                          },
                          'job': {
                              'endpoints': {
                                  '$job': {
                                      'methods': {
                                          'delete': [ 204 ],
                                          'put': [ 204 ]
                                      }
                                  }
                              }
                          },
                          'task': {
                              'endpoints': {
                                  'kill': {
                                      'endpoints': {
                                          '$task': {
                                              'methods': {
                                                  'delete': [ 204 ]
                                              }
                                          }
                                      }
                                  }
                              }
                          },
                          'dependency': {
                              'methods': {
                                  'post': [ 204 ]
                              }
                          },
                          'iso8601': {
                              'methods': {
                                  'post': [ 204 ]
                              }
                          }
                      }
                  }
              }

example_endpoint = { 'jobs': example_api['scheduler']['endpoints']['jobs'] }

def endpoint_get(dictionary, path):
    current = dictionary
    endpoints = {}

    for key in path:
        next = False
        if key in current:
            current = current.get(key)
            endpoints = current.get('endpoints')
            next = True
        elif key in endpoints:
            current = endpoints.get(key)
            endpoints = current.get('endpoints')
            next = True
        elif len(current.keys()) == 1 or len(endpoints.keys()) == 1:
            for k in current.keys():
                if k.startswith('$'):
                    current = current.get(k)
                    endpoints = current.get('endpoints')
                    next = True
                    break
            for k in endpoints.keys():
                if k.startswith('$'):
                    current = endpoints.get(k)
                    endpoints = current.get('endpoints')
                    next = True
                    break
        if not next:
            return None

    return current

def mock_request(method, url, params=None, data=None, headers=None, auth=None):
    endpoint = endpoint_get(example_api, url.split('/')[3:])

    mock_response = mock.Mock()

    if endpoint:
        accepted_code = endpoint.get('methods', {}).get(method, [403])[0]
        mock_response.status_code = accepted_code
    else:
        mock_response.status_code = 404

    return mock_response

class EndpointTestCase(unittest.TestCase):
    def setUp(self):
        self.test_endpoint = example_endpoint.copy()

    def test_creation_from_dictionary(self):
        name, specification = self.test_endpoint.popitem()
        endpoint = Endpoint(name, **specification)

        self.assertIsInstance(endpoint, Endpoint)

class APITestCase(unittest.TestCase):
    def setUp(self):
        self.test_api = example_api.copy()

    def test_creation_from_dictionary(self):
        api = API(**self.test_api)

        self.assertIsInstance(api, API)

        self.assertIn('scheduler', api)

        for key in example_api['scheduler']['endpoints'].keys():
            self.assertIn(key, api.scheduler)

class ClientTestCase(unittest.TestCase):
    def setUp(self):
        self.test_api = API(**example_api)

    @mock.patch('requests.sessions.Session.request')
    def test_creation(self, mock_session_request):
        mock_session_request.side_effect = mock_request

        test_host = 'https://1.2.3.4:1234'

        client = Client(test_host, self.test_api)

        client.api.scheduler.jobs.get()
        mock_session_request.assert_called_with(url='%s/scheduler/jobs' % test_host, method='get')

        client.api.scheduler.job.job('abc').put(data={})
        mock_session_request.assert_called_with(url='%s/scheduler/job/abc' % test_host, method='put', data={})

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
